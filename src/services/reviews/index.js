const { Review } = require('../../models');
const LIMIT = 5;

const getAll = async (query) => {
  let {
    page = 1,
    limit = LIMIT,
    sort = 'createdAt',
    search = '',
    tours,
  } = query;
  page = parseInt(page);
  limit = parseInt(limit);
  limit >= LIMIT ? LIMIT : limit;
  const offset = (page - 1) * limit;

  const data = await Review.find({
    reviews: { $regex: `.*${search}.*`, $options: 'i' },
    tours,
  })
    .skip(offset)
    .limit(limit)
    .select('reviews ratings tours users')
    .sort(sort);

  const count = await Review.countDocuments();
  const totalPages = Math.ceil(count / limit);
  return {
    message: 'Get all reviews',
    status: 200,
    data,
    page,
    limit,
    totalPages,
    totalDocs: count,
  };
};

const createReview = async (body) => {
  await Review.create({ ...body });
  return {
    message: 'Created reviews ok!',
    status: 201,
  };
};

const findAll = async (query) => {
  let { page = 1, limit = LIMIT, sort = 'createdAt', search = '' } = query;
  const offset = (page - 1) * limit;
  const data = await Review.find({
    reviews: { $regex: `.*${search}.*`, $options: 'i' },
  })
    .skip(offset)
    .limit(limit)
    .select('reviews ratings tours users')
    .sort(sort);

  const count = await Review.countDocuments();
  const totalPages = Math.ceil(count / limit);
  return {
    message: 'Find all reviews ok',
    status: 200,
    data,
    page,
    limit,
    totalDocs: count,
    totalPages,
  };
};

const update = async (id, body) => {
  await Review.findByIdAndUpdate(id, { ...body });
  return {
    message: 'Update successfully',
    status: 200,
  };
};

module.exports = { getAll, createReview, findAll, update };
