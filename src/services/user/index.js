const { UserModel } = require('../../models');
const { filterObj } = require('../../utils/helpers');

const updateMe = async (req) => {
  const currentUser = req.user;
  if (req.body.password)
    return {
      message: 'This route can not update password',
      statusCode: 400,
    };
  const filterBody = filterObj(req.body, 'name', 'email');
  if (req.file) filterBody.photo = req.file.filename;
  const updatedUser = await UserModel.findByIdAndUpdate(
    currentUser._id,
    filterBody,
    { new: true }
  );
  return {
    message: 'Update user successfull',
    statusCode: 200,
    user: updatedUser,
  };
};

const deleteMyself = async ({ user }) => {
  await UserModel.findByIdAndUpdate(user.id, { active: false });
  return {
    message: 'Remove user successsfully',
    statusCode: 200,
  };
};

const getMe = async (id) => {
  const user = await UserModel.findById(id).select('name email role photo');
  return {
    message: 'Get me ok',
    status: 200,
    user,
  };
};

module.exports = { updateMe, deleteMyself, getMe };
