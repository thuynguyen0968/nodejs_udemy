const { UserModel } = require('../../models');
const { generateToken } = require('../../utils/helpers');
const bcrypt = require('bcrypt');
const SendEmailProd = require('../../utils/realMail');
const crypto = require('crypto');

const register = async (req) => {
  const url = `${req.protocol}://${req.get('host')}/profile`;
  const hash = await bcrypt.hash(req.body.password, 10);
  const email = new SendEmailProd(req.body, url);

  await UserModel.create({ ...req.body, password: hash });
  await email.sendWellcome();
  return {
    status: 'success',
    statusCode: 201,
    message: 'Register successfully',
  };
};

const queryUsers = async () => {
  const users = await UserModel.find();
  return {
    message: 'Get all users',
    status: 'success',
    statusCode: 200,
    users,
  };
};

const login = async (body) => {
  let user = await UserModel.findOne(
    { email: body.email },
    'email name photo role'
  ).select('+password');
  if (!user)
    return {
      message: 'Email or password not correct',
      status: 'fail',
      statusCode: 400,
    };

  const matched = await bcrypt.compare(body.password, user.password);
  const payload = { email: user.email, name: user.name };
  delete user._doc.password;
  if (matched) {
    const accessToken = generateToken(payload);
    return {
      message: 'Login successfully!',
      status: 'success',
      statusCode: 200,
      user,
      accessToken,
    };
  }
  return {
    message: 'Email or password not correct',
    status: 'fail',
    statusCode: 400,
  };
};

const forgot = async (req) => {
  // 1. find user by email
  const user = await UserModel.findOne({ email: req.body.email });
  if (!user)
    return {
      message: 'No user not found',
      statusCode: 404,
    };
  // 2. random reset token
  const resetToken = user.resetPasswordToken();
  await user.save();

  // 3. send email set password
  const resetURL = `${req.protocol}://${req.get(
    'host'
  )}/auth/reset/${resetToken}`;

  const message = `Reset your password to login`;

  const emailReset = new SendEmailProd(user, resetURL);
  await emailReset.sendPassword(message);

  return {
    message: 'Send email to reset password ok!!',
    statusCode: 200,
  };
};
const reset = async (req) => {
  // 1. get base user on token
  const hashedToken = crypto
    .createHash('sha256')
    .update(req.params.token)
    .digest('hex');
  const user = await UserModel.findOne({
    passwordResetToken: hashedToken,
    passwordExpires: { $gt: Date.now() },
  });
  if (!user)
    return {
      message: 'Token is invalid or has expired',
      statusCode: 400,
    };

  const hash = await bcrypt.hash(req.body.password, 10);
  user.password = hash;
  user.passwordResetToken = undefined;
  user.passwordExpires = undefined;
  await user.save();

  const payload = { email: user.email, name: user.name };
  const token = generateToken(payload);

  return {
    message: 'Reset password success',
    statusCode: 200,
    accessToken: token,
  };
};

const update = async ({ id, password, newPassword }) => {
  // 1. get current user
  const currentUser = await UserModel.findById(id).select('+password');
  //2. check password client send
  const matched = await bcrypt.compare(password, currentUser.password);

  // 3. update password if correct
  if (matched) {
    const hash = await bcrypt.hash(newPassword, 10);
    currentUser.password = hash;
    await currentUser.save();

    const payload = { email: currentUser.email, name: currentUser.name };
    const token = generateToken(payload);

    return {
      message: 'Update password success',
      statusCode: 200,
      accessToken: token,
    };
  }
  return {
    message: 'Password not correct, type again',
    statusCode: 400,
  };
};

module.exports = {
  register,
  login,
  queryUsers,
  forgot,
  reset,
  update,
};
