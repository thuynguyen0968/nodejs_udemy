/* eslint-disable no-debugger */
const { tourModel } = require('../../models');
const LIMIT = 5;

const getAll = async (query) => {
  // Avanced querystring
  // let queryStr = JSON.stringify(query);
  // queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);

  let { page = 1, limit = LIMIT, sort = 'createdAt', search = '' } = query;
  page = parseInt(page);
  limit = parseInt(limit) && limit >= LIMIT ? LIMIT : limit;
  const offset = (page - 1) * limit;

  // sorting
  const data = await tourModel
    .find({ name: { $regex: `.*${search}.*`, $options: 'i' } })
    .skip(offset)
    .limit(limit)
    .sort(sort);

  const count = await tourModel.countDocuments();

  return {
    message: 'Get all tours',
    status: 200,
    tours: data,
    results: data.length,
    page,
    totalPages: Math.ceil(count / limit),
    totalTours: count,
  };
};

const getOne = async (id) => {
  const tour = await tourModel.findById(id).populate({
    path: 'review',
    select: 'reviews',
  });
  if (tour)
    return {
      message: 'Get tour details ok!!',
      statusCode: 200,
      tour,
    };
  return { message: 'Tour not found', statusCode: 404 };
};

const createNewTour = async (body) => {
  await tourModel.create({ ...body });
  return { message: 'Create tours ok!!', statusCode: 201 };
};

const updateTours = async (id, body) => {
  console.log('🚀 ~ updateTours ~ body:', body);
  await tourModel.findByIdAndUpdate(id, body);
  return { message: 'Update tour success', statusCode: 200 };
};

const getToursVerTwo = async (query) => {
  let { search = '', sort = 'createdAt', limit = LIMIT, page = 1 } = query;
  page = parseInt(page);
  limit = parseInt(limit);
  limit >= LIMIT ? LIMIT : limit;

  const offset = (page - 1) * limit;

  const data = await tourModel
    .aggregate([
      {
        $match: { name: { $regex: `.*${search}.*`, $options: 'i' } },
      },
      /* {
        $group: {
          _id: 'difficulty',
          totalTours: { $sum: 1 },
          avgPrice: { $avg: '$price' },
        },
      }, */
      { $skip: offset },
      { $limit: limit },
    ])
    .sort(sort);

  return {
    message: 'Get all tours ok!!',
    status: 200,
    size: data.length,
    page,
    tours: data,
  };
};

const getTourWith = async (params) => {
  let { distance, coor, unit } = params;
  distance = parseInt(distance);
  const [lat, lng] = coor.split(',');
  const radius = unit === 'mi' ? distance / 3963.2 : distance / 6378.1;

  if (!lat || !lng) {
    return {
      message: 'Please provide coors of the tour',
      status: 404,
    };
  }
  const tours = await tourModel.find({
    startLocation: { $geoWithin: { $centerSphere: [[lng, lat], radius] } },
  });

  return {
    message: 'Get tours ok!!',
    satus: 200,
    results: tours.length,
    tours,
  };
};

module.exports = {
  getAll,
  createNewTour,
  getOne,
  updateTours,
  getToursVerTwo,
  getTourWith,
};
