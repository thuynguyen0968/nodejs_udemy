/* eslint-disable no-undef */
const { tourModel, Booking } = require('../../models');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const checkoutFn = async (req) => {
  const tour = await tourModel.findById(req.params.tourId);

  const session = await stripe.checkout.sessions.create({
    payment_method_types: ['card'],
    success_url: `${req.protocol}://${req.get('host')}?tour=${
      req.params.tourId
    }&token=${req.user.id}&price=${tour.price}`,
    cancel_url: `${req.protocol}://${req.get('host')}/tour`,
    customer_email: req.user.email,
    client_reference_id: req.params.tourId,
    line_items: [
      {
        price_data: {
          currency: 'usd',
          unit_amount: tour.price * 100,
          product_data: {
            name: tour.name,
            description: tour.summary,
            images: [
              `https://placehold.co/600x400/orange/white?text=${tour.name}`,
            ],
          },
        },
        quantity: 1,
      },
    ],
    mode: 'payment',
  });

  return {
    message: 'Checkout sucessfully!!',
    status: 201,
    session,
  };
};

const createBookings = async (query) => {
  let { tour, token, price } = query;
  price = parseInt(price);
  await Booking.create({ tour, user: token, price });
};

const getAllBooking = async () => {
  const bookings = await Booking.find();
  return {
    message: 'Get all booking',
    status: 200,
    data: bookings,
  };
};

const destroyBookings = async (id) => {
  await Booking.findOneAndDelete({ tour: id });
  return {
    message: 'Delete booking ok!',
    status: 200,
  };
};

const getTours = async (user) => {
  const data = await Booking.find({ user: user.id });
  const toursId = data.map((el) => el.tour);
  const tours = await tourModel.find({ _id: { $in: toursId } });

  return {
    message: 'Get all tour booked',
    status: 200,
    tours,
  };
};

module.exports = {
  checkoutFn,
  createBookings,
  getTours,
  getAllBooking,
  destroyBookings,
};
