const AppError = require('../utils/appErr');
const { verifyToken, changePasswordAfter } = require('../utils/helpers');
const { handlerError } = require('../controllers/error');
const { UserModel } = require('../models');
const multer = require('multer');
const sharp = require('sharp');

// const multerStorage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     cb(null, 'public/upload');
//   },
//   filename: (req, file, cb) => {
//     const ext = file.mimetype.split('/')[1];
//     cb(null, `user-${req.user._id}-${Date.now()}.${ext}`);
//   },
// });

const multerStorage = multer.memoryStorage();

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith('image')) {
    cb(null, true);
  } else {
    cb(new AppError('Not an image!Please upload again!!', 400), false);
  }
};
const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});

const resizeImage = (req, res, next) => {
  if (!req.file) {
    return res.status(404).json({
      message: 'Choose an image to upload',
      status: 404,
    });
  }

  req.file.filename = `user-${Date.now()}.jpeg`;

  sharp(req.file.buffer)
    .resize({ width: 500, height: 500 })
    .toFormat('jpeg')
    .jpeg({ quality: 90 })
    .toFile(`public/upload/${req.file.filename}`);

  next();
};

const uploadTourImgs = upload.fields([{ name: 'images', maxCount: 3 }]);

const resizeTourImgs = async (req, res, next) => {
  req.body.images = [];
  if (!req.files.images) {
    return res.status(404).json({
      message: 'Choose images to upload',
      status: 404,
    });
  }

  const resizePromises = req.files.images.map(async (file) => {
    const filename = `tour-${req.params.id}-${Date.now()}.jpeg`;
    await sharp(file.buffer)
      .resize({ width: 500, height: 500 })
      .toFormat('jpeg')
      .jpeg({ quality: 90 })
      .toFile(`public/upload/${filename}`);

    req.body.images.push(filename);
  });

  await Promise.all(resizePromises);
  next();
};

const myMiddleware = (req, res, next) => {
  req.requestTime = new Date().toLocaleString();
  next();
};

const idMiddleware = (req, res, next, value) => {
  req.params.id = value;
  console.log('Hello from id middleware 🖐🏻🖐🏻');
  next();
};

const protectedRoutes = async (req, res, next) => {
  try {
    // 1. Check token in headers
    const token =
      req.headers['authorization'] &&
      req.headers['authorization'].split(' ')[1];
    if (!token) {
      return next(new AppError('Your are not to login', 401));
    }
    // 2. verify tokens
    const decoded = await verifyToken(token);

    // 3 . find current users, check valid token
    const currentUser = await UserModel.findOne({ email: decoded.email });
    if (!currentUser)
      return next(new AppError('This token no longer exists', 403));

    // 4. check user has change password
    const isChangePass = changePasswordAfter(decoded.iat, currentUser);
    if (isChangePass) {
      next(new AppError('User has change password, please login again'));
    }

    // pass all case -> next()
    req.user = currentUser;
    next();
  } catch (err) {
    handlerError(err, req, res);
  }
};

const checkAccount = (req, res, next) => {
  if (!req.user.active) {
    return res.status(404).json({
      message: 'Your account has been deleted',
      status: 'fail',
    });
  }
  next();
};

const permissionAction = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return res
        .status(403)
        .json({ message: 'Permission denined', status: 'fail' });
    }
    next();
  };
};

module.exports = {
  myMiddleware,
  idMiddleware,
  protectedRoutes,
  permissionAction,
  checkAccount,
  upload,
  multerStorage,
  multerFilter,
  resizeImage,
  uploadTourImgs,
  resizeTourImgs,
};
