/* eslint-disable no-undef */
const pug = require('pug');
const nodemailer = require('nodemailer');
const htmlToText = require('html-to-text');

class SendMailProd {
  constructor(user, url) {
    this.to = `${user.name} ${user.email}`;
    this.firstName = user.name.split(' ')[0];
    this.url = url;
    this.from = `"Thuy Nguyen" <${process.env.EMAIL_FROM}>`;
  }

  newTransport() {
    const transporter = nodemailer.createTransport({
      host: process.env.SENDGRID_HOST,
      port: process.env.SENDGRID_PORT,
      auth: {
        user: 'apikey',
        pass: process.env.SENDGRID_KEY,
      },
    });

    return transporter;
  }

  async sendRealMail(template, subject) {
    const html = pug.renderFile(
      `${__dirname}/../views/emails/${template}.pug`,
      {
        firstName: this.firstName,
        url: this.url,
        subject,
      }
    );

    const mailOptions = {
      from: this.from,
      to: this.to,
      subject: subject,
      html,
      text: htmlToText.convert(html),
    };

    await this.newTransport().sendMail(mailOptions);
  }

  async sendWellcome() {
    await this.sendRealMail('wellcome', 'Wellcome to the Natours Family');
  }

  async sendPassword(message) {
    await this.sendRealMail('reset', message);
  }
}
module.exports = SendMailProd;
