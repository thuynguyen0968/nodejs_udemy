class AppError extends Error {
  constructor(message, statusCode) {
    super(message);

    this.statusCode = statusCode;
    this.status = `${this.statusCodde}`.startsWith('4') ? 'fail' : 'error';
    this.isOperator = true;
  }
}

module.exports = AppError;
