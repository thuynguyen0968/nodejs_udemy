/* eslint-disable no-undef */
const nodemailer = require('nodemailer');
const pug = require('pug');
const htmlToText = require('html-to-text');
class Email {
  constructor(user, url) {
    this.to = `${user.name} ${user.email}`;
    this.firstName = user.name.split(' ')[0];
    this.url = url;
    this.from = `"John Smith" <${process.env.EMAIL_FROM}>`;
  }

  newTransport() {
    const transporter = nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASS,
      },
    });

    return transporter;
  }

  async send(template, subject) {
    const html = pug.renderFile(
      `${__dirname}/../views/emails/${template}.pug`,
      {
        firstName: this.firstName,
        url: this.url,
        subject,
      }
    );

    const mailOptions = {
      from: this.from,
      to: this.to,
      subject: subject,
      html,
      text: htmlToText.convert(html),
    };

    await this.newTransport().sendMail(mailOptions);
  }

  async sendWellcome() {
    await this.send('wellcome', 'Wellcome to the Natours Family');
  }

  async sendPassword(message) {
    await this.send('reset', message);
  }
}
module.exports = Email;
