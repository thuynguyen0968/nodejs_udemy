/* eslint-disable no-shadow-restricted-names */
/* eslint-disable no-undef */
const jwt = require('jsonwebtoken');
const { promisify } = require('util');

function logger(params = null) {
  if (params) {
    console.log(params.toString());
  }
}

const randomId = (num) => Math.floor(Math.random() * num);

const cloneDeep = (params) => {
  return JSON.parse(JSON.stringify(params));
};

const findUserByEmail = async (schema, email) => {
  const user = await schema.findOne({ email }, 'name email');
  if (user) {
    return user;
  }
  return false;
};

const generateURL = (name, options = {}) => {
  const { color = 'orange', bg = 'white' } = options;
  return `https://placehold.co/600x400/${color}/${bg}?text=${name}`;
};

const getValueErr = (message) => {
  const regex = /{[^:]+:\s*"([^"]+)"/;
  const match = regex.exec(message);
  if (match) return match[1];
  return null;
};

const generateToken = (payload) => {
  const tokens = jwt.sign(payload, process.env.JWT_SIGN, {
    expiresIn: process.env.JWT_EXPIRED,
  });

  return tokens;
};

const verifyToken = async (token) => {
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SIGN);
  return decoded;
};

const changePasswordAfter = (jwtTime, user) => {
  if (user.changePassword) {
    const timePassChange = user.changePassword.getTime() / 1000;
    return jwtTime < timePassChange; // change password after generate token
  }
  // false mean not change password
  return false;
};

const filterObj = (obj, ...rest) => {
  const newObj = {};
  Object.keys(obj).forEach((el) => {
    if (rest.includes(el)) newObj[el] = obj[el];
  });

  return newObj;
};

const responseFromServer = (res) => {
  return { ...res };
};

module.exports = {
  logger,
  randomId,
  cloneDeep,
  findUserByEmail,
  generateURL,
  filterObj,
  getValueErr,
  generateToken,
  verifyToken,
  changePasswordAfter,
  responseFromServer,
};
