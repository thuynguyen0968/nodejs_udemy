const { updateMe, deleteMyself, getMe } = require('../../services/user');
const { logger } = require('../../utils/helpers');
const { handlerError } = require('../error');

class UserController {
  async updateUser(req, res) {
    try {
      const data = await updateMe(req);
      res.status(data.statusCode).json({ ...data });
    } catch (err) {
      logger(err);
      handlerError(err, req, res);
    }
  }

  async deleteUser(req, res) {
    const user = req.user;
    try {
      const data = await deleteMyself({ user });
      res.status(data.statusCode).json({ ...data });
    } catch (err) {
      logger(err);
    }
  }

  async getUser(req, res) {
    const id = req.user._id;
    try {
      const data = await getMe(id);
      res.status(data.status).json({ ...data });
    } catch (err) {
      handlerError(err);
    }
  }
}

module.exports = new UserController();
