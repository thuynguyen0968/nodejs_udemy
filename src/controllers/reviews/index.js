const {
  getAll,
  createReview,
  findAll,
  update,
} = require('../../services/reviews');
const { logger } = require('../../utils/helpers');
const { handlerError } = require('../error');

class ReviewController {
  async getReviews(req, res) {
    let filter = {};
    if (req.params.id) filter = { tours: req.params.id };
    const query = { ...filter, ...req.query };
    try {
      const data = await getAll(query);
      res.status(data.status).json({ ...data });
    } catch (err) {
      logger(err);
      handlerError(err, req, res);
    }
  }

  async postReview(req, res) {
    if (!req.body.tours) req.body.tours = req.params.id;
    if (!req.body.users) req.body.users = req.user.id;
    // current user reviewing
    try {
      const data = await createReview(req.body);
      res.status(data.status).json({ ...data });
    } catch (err) {
      logger(err);
      handlerError(err, req, res);
    }
  }

  async getAllReviews(req, res) {
    try {
      const data = await findAll(req.query);
      res.status(data.status).json({ ...data });
    } catch (err) {
      logger(err);
      handlerError(err, req, res);
    }
  }

  async updateReviews(req, res) {
    const id = req.params.id;
    const body = req.body;
    try {
      const data = await update(id, body);
      res.status(data.status).json({ ...data });
    } catch (err) {
      handlerError(err);
    }
  }
}

module.exports = new ReviewController();
