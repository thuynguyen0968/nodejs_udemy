/* eslint-disable no-unused-vars */
const { deleteData, importData } = require('../../import/data');
const { tourModel } = require('../../models');
const {
  getAll,
  getOne,
  updateTours,
  getToursVerTwo,
  getTourWith,
} = require('../../services/natrous');
const { logger } = require('../../utils/helpers');
const { handlerError } = require('../error');
const { deleteOneById, createOneDoc } = require('../factory');

class ToursControllers {
  async aliasTopTours(req, res, next) {
    try {
      const { limit = 5, sort = 'price' } = req.query;
      next();
    } catch (error) {
      logger(error);
    }
  }

  async getTours(req, res, next) {
    const includesField = { ...req.query };

    try {
      const data = await getAll(includesField);
      res.status(data.status).json({ time: req.requestTime, ...data });
    } catch (err) {
      next(err);
    }
  }

  async getDetail(req, res) {
    try {
      const { id } = req.params;
      const data = await getOne(id);
      res.status(data?.statusCode).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async postTour(req, res) {
    try {
      const data = await createOneDoc(tourModel, req.body);
      res.status(data.statusCode).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async patchTour(req, res) {
    try {
      const { id } = req.params;
      const data = await updateTours(id, req.body);
      res.status(data.statusCode).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async destroyTour(req, res) {
    const { id } = req.params;
    try {
      const data = await deleteOneById(tourModel, id);
      res.status(data.status).json({ ...data });
    } catch (err) {
      logger(err);
      res.status(500).json(err);
    }
  }

  async getTourStart(req, res) {
    try {
      const data = await getToursVerTwo(req.query);
      res.status(data.status).json({ ...data });
    } catch (err) {
      logger(err);
      res.status(404).json(err);
    }
  }

  async clearAllTours(req, res) {
    try {
      await deleteData();
      res.status(200).json({ message: 'Delete tours ok!!' });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async insertData(req, res) {
    try {
      await importData();
      res.status(200).json({ message: 'Import all tours ok!!' });
    } catch (err) {
      logger(err);
      handlerError(err, req, res);
    }
  }

  async tourWithThin(req, res) {
    try {
      const data = await getTourWith(req.params);
      res.status(data.satus).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }
}

module.exports = new ToursControllers();
