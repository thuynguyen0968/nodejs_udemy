const {
  checkoutFn,
  createBookings,
  getTours,
  getAllBooking,
  destroyBookings,
} = require('../../services/bookings');
const { logger } = require('../../utils/helpers');
const { handlerError } = require('../error');

class BookingsController {
  async getCheckoutSession(req, res) {
    try {
      const data = await checkoutFn(req);
      res.status(data.status).json({ ...data });
    } catch (err) {
      logger(err);
      handlerError(err, req, res);
    }
  }

  async createBookingCheckout(req, res, next) {
    try {
      if (!req.query) return next();
      await createBookings(req.query);
      res.redirect(req.originalUrl.split('?')[0]);
    } catch (err) {
      logger(err);
      handlerError(err, req, res);
    }
  }

  async getMyTour(req, res) {
    try {
      const data = await getTours(req.user);
      res.status(data.status).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async getAll(req, res) {
    try {
      const data = await getAllBooking();
      res.status(data.status).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async deleteBookings(req, res) {
    try {
      const data = await destroyBookings(req.params.bookingId);
      res.status(data.status).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }
}

module.exports = new BookingsController();
