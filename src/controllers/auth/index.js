/* eslint-disable no-undef */
const { handlerError } = require('../error');
const {
  register,
  login,
  queryUsers,
  forgot,
  reset,
  update,
} = require('../../services/auth');
const { logger } = require('../../utils/helpers');

const EXPIRES_COOKIES = new Date(Date.now() + 30 * 24 * 60 * 60 * 1000);

class authController {
  async registerUser(req, res) {
    try {
      const response = await register(req);
      res.status(response.statusCode).json({ ...response });
    } catch (err) {
      console.log(err);
      handlerError(err, req, res);
    }
  }

  async loginUser(req, res) {
    const cookieOptions = {
      httpOnly: true,
      expires: EXPIRES_COOKIES,
    };
    if (process.env.NODE_ENV === 'production') cookieOptions.secure = true;
    try {
      const data = await login(req.body);
      res.cookie('token', data.accessToken, cookieOptions);
      res.status(data.statusCode).json({ loginAt: req.requestTime, ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async getAllUsers(req, res) {
    try {
      const data = await queryUsers();
      res.status(data.statusCode).json({ ...data });
    } catch (err) {
      logger(err);
      handlerError(err, req, res);
    }
  }

  async forgotPassword(req, res) {
    try {
      const data = await forgot(req);
      res.status(data.statusCode).json({ ...data });
    } catch (err) {
      logger(err);
      handlerError(err, req, res);
    }
  }

  async resetPassword(req, res) {
    try {
      const data = await reset(req);
      res.status(data.statusCode).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }

  async updatePassword(req, res) {
    const id = req.user._id;
    const { password, newPassword } = req.body;
    try {
      const data = await update({ id, password, newPassword });
      res.status(data.statusCode).json({ ...data });
    } catch (err) {
      handlerError(err, req, res);
    }
  }
}

module.exports = new authController();
