const ToursCtrl = require('./natrous/tours');
const authCtrl = require('./auth');
const userCtrl = require('./user');
const reviewCtrl = require('./reviews');
const bookingCtrl = require('./bookings');

module.exports = { ToursCtrl, authCtrl, userCtrl, reviewCtrl, bookingCtrl };
