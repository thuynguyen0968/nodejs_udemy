const deleteOneById = async (Model, id) => {
  const doc = await Model.findByIdAndDelete(id);
  if (!doc)
    return {
      message: 'No document was found!!',
      status: 404,
    };
  return {
    message: 'Delete documents ok!!',
    status: 200,
  };
};

const createOneDoc = async (Model, body) => {
  await Model.create({ ...body });
  return { message: 'Create document ok!!', statusCode: 201 };
};

module.exports = { deleteOneById, createOneDoc };
