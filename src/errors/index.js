/* eslint-disable no-undef */
const handleCatException = () => {
  process.on('uncaughtException', (err) => {
    console.log(err.message);
    process.exit(1);
  });
};

const handleUnRejection = (server) => {
  process.on('unhandledRejection', (err) => {
    console.log(err.message);
    server.close(() => {
      process.exit(1);
    });
  });
};

module.exports = { handleCatException, handleUnRejection };
