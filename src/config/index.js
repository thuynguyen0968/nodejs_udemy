const cors = require('cors');
const cookieParser = require('cookie-parser');
const express = require('express');
const { rateLimit } = require('express-rate-limit');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000,
  max: 100,
  standardHeaders: true,
  legacyHeaders: false,
  message: 'To many request from this IP, try again late',
});

const defineConfig = (app) => {
  app.use(helmet());
  app.use(mongoSanitize());
  app.use(xss());
  app.use(cors({ origin: true }));
  app.use(express.json());
  app.use(cookieParser());

  app.use(express.static('public'));
  app.use(limiter);
};
module.exports = defineConfig;
