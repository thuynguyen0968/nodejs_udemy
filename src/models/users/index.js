const { Schema, model } = require('mongoose');
const validator = require('validator');
const crypto = require('crypto');

const schema = new Schema(
  {
    name: {
      type: String,
      required: true,
      minLength: [5, 'Name must be less than more 5 characters'],
    },
    email: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
      validate: [validator.isEmail, 'Please type valid email'],
    },
    password: {
      type: String,
      required: true,
      minLength: [5, 'Passwsord must be less than 5 characters'],
      select: false,
    },
    photo: {
      type: String,
      default: 'default.jpg',
    },
    changePassword: Date,
    role: {
      type: String,
      enum: ['user', 'guide', 'leader', 'admin'],
      default: 'user',
    },
    passwordResetToken: {
      type: String,
    },
    passwordExpires: {
      type: Date,
    },
    active: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

schema.pre('save', function (next) {
  if (!this.isModified('password') || this.isNew) return next();
  this.changePassword = Date.now() - 1000;
  next();
});

schema.pre(/^find/, function (next) {
  this.find({ active: { $ne: false } });
  next();
});

schema.methods.resetPasswordToken = function () {
  const resetToken = crypto.randomBytes(32).toString('hex');
  this.passwordResetToken = crypto
    .createHash('sha256')
    .update(resetToken)
    .digest('hex');

  this.passwordExpires = Date.now() + 10 * 60 * 1000;

  return resetToken;
};

const UserModel = model('User', schema);
module.exports = UserModel;
