const { Schema, model } = require('mongoose');
const slugify = require('slugify');

const toursSchema = new Schema(
  {
    name: {
      type: String,
      minLength: [8, 'Name must be less than more 8 characters'],
      required: [true, 'Missing name of tour'],
      unique: true,
    },
    price: {
      type: Number,
      required: [true, 'A tour must be has price'],
    },
    durations: {
      type: Number,
    },
    difficulty: {
      type: String,
      default: 'medium',
      enum: ['medium', 'easy', 'difficult'],
    },
    maxGroupsize: {
      type: Number,
    },
    ratings: {
      type: Number,
      default: 4.5,
    },
    ratingsAverage: {
      type: Number,
      min: 1,
      max: 5,
    },
    ratingsQuantity: {
      type: Number,
    },
    priceDiscount: {
      type: Number,
      validate: {
        validator: function (val) {
          return val < this.price;
        },
        message: "Discount coun't large than price",
      },
    },
    summary: {
      type: String,
    },
    slug: {
      type: String,
    },
    secret: {
      type: Boolean,
      default: false,
    },
    startLocation: {
      type: {
        type: String,
        default: 'Point',
        enum: ['Point'],
      },
      coordinates: [Number],
      addr: String,
      description: String,
    },
    locations: [
      {
        type: {
          type: String,
          default: 'Point',
          enum: ['Point'],
        },
        coordinates: [Number],
        addr: String,
        description: String,
      },
    ],
    guides: [
      {
        type: Schema.ObjectId,
        ref: 'User',
      },
    ],
    images: [String],
  },
  { timestamps: true, toJSON: { virtuals: true }, toObject: { virtuals: true } }
);

toursSchema.virtual('priceVND').get(function () {
  const vnd = this.price * 23000;
  return vnd.toLocaleString('vi', {
    style: 'currency',
    currency: 'VND',
  });
});

toursSchema.virtual('review', {
  ref: 'Review',
  foreignField: 'tours',
  localField: '_id',
});

// toursSchema.pre('save', async function (next) {
//   const guidesPromise = this.guides.map(
//     async (id) => await UserModel.findById(id)
//   );
//   this.guides = await Promise.all(guidesPromise);
//   next();
// });

toursSchema.index({ name: 1 });
toursSchema.index({ startLocation: '2dsphere' });

toursSchema.pre(/^find/, function (next) {
  this.start = Date.now();
  next();
});

toursSchema.pre(/^find/, function (next) {
  this.populate({
    path: 'guides',
    select: 'name email role',
  });

  next();
});

toursSchema.post(/^find/, function (docs, next) {
  console.log(`Qerry took ${Date.now() - this.start} ms`);
  next();
});

toursSchema.pre('save', function (next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});
const tourModel = model('Tour', toursSchema);

module.exports = tourModel;
