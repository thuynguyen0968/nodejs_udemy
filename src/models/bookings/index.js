const { Schema, model } = require('mongoose');

const schema = new Schema(
  {
    tour: {
      type: Schema.ObjectId,
      ref: 'Tour',
    },
    user: {
      type: Schema.ObjectId,
      ref: 'User',
    },
    price: Number,
    paid: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

schema.pre(/^find/, function (next) {
  this.populate({
    path: 'tour',
    select: 'name summary price',
  }).populate({
    path: 'user',
    select: 'name email role',
  });

  next();
});

const Booking = model('Booking', schema);
module.exports = Booking;
