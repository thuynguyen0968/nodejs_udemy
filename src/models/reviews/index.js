// reviews / ratings / time / ref to tours and user
const { Schema, model } = require('mongoose');
const tourModel = require('../tours');

const schema = new Schema(
  {
    reviews: {
      type: String,
      required: [true, 'Review can not be empty'],
    },
    ratings: {
      type: Number,
      required: true,
      min: 1,
      max: 5,
    },
    tours: {
      type: Schema.ObjectId,
      required: [true, 'Reviews must belong to a tour'],
      ref: 'Tour',
    },
    users: {
      type: Schema.ObjectId,
      required: [true, 'A reviews belong an user'],
      ref: 'User',
    },
  },
  { timestamps: true, toJSON: { virtuals: true }, toObject: { virtuals: true } }
);

schema.index({ tours: 1, users: 1 }, { unique: true });

schema.statics.calAvgRatings = async function (tourId) {
  const stats = await this.aggregate([
    {
      $match: { tours: tourId },
    },
    {
      $group: {
        _id: '$tours',
        nRating: { $sum: 1 },
        avgRating: { $avg: '$ratings' },
      },
    },
  ]);

  await tourModel.findByIdAndUpdate(tourId, {
    ratingsQuantity: stats[0].nRating,
    ratingsAverage: stats[0].avgRating,
  });
};

schema.pre(/^find/, function (next) {
  this.populate({
    path: 'users',
    select: 'name',
  });
  next();
});

schema.post('save', function () {
  this.constructor.calAvgRatings(this.tours);
});
const Reviews = model('Review', schema);

module.exports = Reviews;
