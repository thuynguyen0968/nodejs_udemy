const tourModel = require('./tours');
const UserModel = require('./users');
const Review = require('./reviews');
const Booking = require('./bookings');

module.exports = { tourModel, UserModel, Review, Booking };
