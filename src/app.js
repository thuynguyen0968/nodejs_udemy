/* eslint-disable no-undef */
const express = require('express');
const defineConfig = require('./config');
const defineRoutes = require('./routes');
const connectDb = require('./db');
const dotenv = require('dotenv');
const { handleCatException, handleUnRejection } = require('./errors');
const path = require('path');

dotenv.config();
handleCatException();

const app = express();
const PORT = process.env.PORT;

const SERVER = app.listen(PORT, () =>
  console.log('Yah, sever are running....')
);
// eslint-disable-next-line no-undef
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

defineConfig(app);
defineRoutes(app);
connectDb();
handleUnRejection(SERVER);
