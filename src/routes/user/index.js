const { Router } = require('express');
const { userCtrl } = require('../../controllers');
const { upload, resizeImage } = require('../../middleware');

const userRoutes = Router();

userRoutes.patch(
  '/update',
  upload.single('photo'),
  resizeImage,
  userCtrl.updateUser
);

userRoutes.delete('/destroy', userCtrl.deleteUser);

userRoutes.get('/', userCtrl.getUser);

module.exports = userRoutes;
