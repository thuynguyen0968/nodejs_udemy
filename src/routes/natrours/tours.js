const toursRoutes = require('express').Router();
const { ToursCtrl } = require('../../controllers');
const {
  myMiddleware,
  idMiddleware,
  permissionAction,
  uploadTourImgs,
  resizeTourImgs,
} = require('../../middleware');
const reviewRoutes = require('../reviews');

toursRoutes.param('id', idMiddleware);

toursRoutes.get('/', myMiddleware, ToursCtrl.getTours);

toursRoutes.get(
  '/top-5-cheap',
  myMiddleware,
  ToursCtrl.aliasTopTours,
  ToursCtrl.getTours
);

toursRoutes
  .route('/tour-with/:distance/center/:coor/unit/:unit')
  .get(ToursCtrl.tourWithThin);

toursRoutes.delete('/clear', ToursCtrl.clearAllTours);

toursRoutes.post('/insert', ToursCtrl.insertData);

toursRoutes.get('/tour-stats', myMiddleware, ToursCtrl.getTourStart);

toursRoutes
  .route('/:id')
  .get(ToursCtrl.getDetail)
  .patch(
    permissionAction('admin', 'leader'),
    uploadTourImgs,
    resizeTourImgs,
    ToursCtrl.patchTour
  )
  .delete(permissionAction('admin', 'leader'), ToursCtrl.destroyTour);

toursRoutes.use('/:id/reviews', reviewRoutes);

module.exports = toursRoutes;
