const { Router } = require('express');
const { authCtrl } = require('../../controllers');
const { protectedRoutes, permissionAction } = require('../../middleware');

const authRoute = Router();

authRoute.get(
  '/',
  protectedRoutes,
  permissionAction('admin'),
  authCtrl.getAllUsers
);

authRoute.post('/register', authCtrl.registerUser);

authRoute.post('/login', authCtrl.loginUser);

authRoute.post('/forgot', authCtrl.forgotPassword);

authRoute.patch('/reset/:token', authCtrl.resetPassword);

authRoute.patch('/updatePassword', protectedRoutes, authCtrl.updatePassword);

module.exports = authRoute;
