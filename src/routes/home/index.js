const { Router } = require('express');
const { bookingCtrl } = require('../../controllers');

const homeRoutes = Router();

homeRoutes.get('/', bookingCtrl.createBookingCheckout, (req, res) => {
  res.render('home.pug');
});

homeRoutes.get('/profile', (req, res) => {
  res.render('user');
});

homeRoutes.get('/tour', (req, res) => {
  res.render('cancer');
});

homeRoutes.post('/', (req, res) => {
  const data = req.body;
  res.status(201).json({ ...data });
});

module.exports = homeRoutes;
