const routes = require('express').Router();
const { bookingCtrl } = require('../../controllers');
const { permissionAction } = require('../../middleware');

routes.get('/checkout-session/:tourId', bookingCtrl.getCheckoutSession);

routes.get('/my-tour', bookingCtrl.getMyTour);

routes.route('/').get(permissionAction('admin', 'leader'), bookingCtrl.getAll);

module.exports = routes;
