const homeRoutes = require('./home');
const toursRoutes = require('./natrours/tours');
const AppError = require('../utils/appErr');
const authRoute = require('./auth');
const userRoutes = require('./user');
const reviewRoute = require('./reviews');
const bookingRoutes = require('./bookings');

const {
  myMiddleware,
  protectedRoutes,
  checkAccount,
} = require('../middleware');

const defineRoutes = (app) => {
  app.use('/', homeRoutes);

  app.use('/api/v1/tours', protectedRoutes, checkAccount, toursRoutes);

  app.use('/auth', myMiddleware, authRoute);

  app.use('/me', protectedRoutes, checkAccount, userRoutes);

  app.use('/reviews', protectedRoutes, checkAccount, reviewRoute);

  app.use('/bookings', protectedRoutes, checkAccount, bookingRoutes);

  app.all('*', (req, res, next) => {
    next(new AppError(`Can not find ${req.url} on server`, 404));
  });
};
module.exports = defineRoutes;
