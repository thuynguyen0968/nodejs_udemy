const { Router } = require('express');
const { reviewCtrl } = require('../../controllers');
const {
  permissionAction,
  protectedRoutes,
  checkAccount,
} = require('../../middleware');
const routes = Router({ mergeParams: true });

routes
  .route('/')
  .get(reviewCtrl.getReviews)
  .post(
    protectedRoutes,
    checkAccount,
    permissionAction('user'),
    reviewCtrl.postReview
  );

routes
  .route('/:id')
  .patch(
    protectedRoutes,
    checkAccount,
    permissionAction('user'),
    reviewCtrl.updateReviews
  );

routes.route('/all').get(reviewCtrl.getAllReviews);

module.exports = routes;
