const fs = require('fs');
const { tourModel, UserModel, Review } = require('../models');

const tours = JSON.parse(
  fs.readFileSync('src/resources/data/tours.json', 'utf-8')
);
const users = JSON.parse(
  fs.readFileSync('src/resources/data/users.json', 'utf-8')
);
const reviews = JSON.parse(
  fs.readFileSync('src/resources/data/reviews.json', 'utf-8')
);

const importData = async () => {
  await tourModel.insertMany([...tours]);
  // await UserModel.insertMany([...users]);
  // await Review.insertMany([...reviews]);
};

const deleteData = async () => {
  await tourModel.deleteMany();
  // await UserModel.deleteMany();
  // await Review.deleteMany();
  console.log('Delete all tours out DB');
};

module.exports = { importData, deleteData };
